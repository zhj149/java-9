package com.ricky.run;


import com.ricky.inter.LogInter;
import com.ricky.utils.HttpUtils9;

import java.util.ServiceLoader;

/**
 * 测试
 只开放给onelevle_01 其他人访问不到
 exports com.ricky.entity to onelevel_01;
 */
public class TestRoot {
    public static void main(String[] args) {
       HttpUtils9.HttpGet2();//使用基础包中的http工具类
        //使用基础包中的实体   ResultBean未开放给此模块 所以使用不了
      //  System.out.println(ResultBean.getSuccess());
      /*  LogInter tt=new LogInter();
        tt.debug("11111");*/
        providerwith();
    }

    /**
     * 要想此处生效
     * 1.rootmodule中配置

     exports com.ricky.inter;

     //提供抽象类loginter默认的实现为logimpl  一般联合ServiceLoader使用
     provides com.ricky.inter.LogInter with ricky.impl.LogImpl;

     2. onelevel_02配置
     requires rootmodule;
     //配合provide使用  没有的话 serviceload会异常  对应TestRoot.providerwith()
     uses com.ricky.inter.LogInter;
     */
    public static void providerwith(){
        ServiceLoader<LogInter> sl=ServiceLoader.load(LogInter.class);
        sl.forEach(logInter -> {
            logInter.debug("ricky");
        });
    }
}

