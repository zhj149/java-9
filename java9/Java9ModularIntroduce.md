# Java 9 模块化解惑

>Java 9 模块化的一个最大的变化体现在Java结构上。
本文将介绍关于“Java 9模块系统的”高级特性。

## Java 9 模块化
- 我们将探讨关于Java 9模块化系统的以下主题：

    1.介绍
    
    2.JavaSe9之Jigsaw Project

    3.当前Java系统的问题
    
    4.Java9模块系统的特性
    
    5.Jdk9和Jdk8的对比
    
    6.Java9模块化是什么？
    
    7.Java9模块化系统之母
    
    8.Java9和Java8程序的对比。
    

### 介绍

Oracle 将Java 9从2017年3月延期到2017年9月进行发行。

我们知道， Java SE 8提供了三大新特性(加上少量改进和新特性)。

- Lambda 表达式
- Stream API
- Date API

同样地， Java SE 9提供了三大新特性(加上少量改进和新特性)：
- Java模块系统(Jigsaw项目) 
- Java REPL(交互式编程)
- Milling project coin(主要是语法改进的一个项目,Java7的时候就已经开始)

>接下来，我们将谈论关于Oracle的Jigsaw项目。

### Java SE 9 ：Jigsaw项目
> 接下来我们将对Jigsaw项目--> Java SE 9： Java模块系统中的一些新特性,做一个简单的介绍。

> JavaSE9中的Jigsaw项目来源与Oracle项目中一个非常出名的大项目。其实在Java7的版本开始就已经启动。
但是由于变动巨大被延期到JavaSE8，最终是随着17年9月份发布的JavaSE9一起发布。


#### Jigsaw项目的主要目标：

- 模块化JDK

>我们知道，当前JDK系统是很大的。因此他们决定划分JDK成小模块得到一定数量的好处(我们很快会在后边的部分谈论到他们)。

- 模块化源代码

> 当前原始代码jar文件是非常大的，特别是rt.jar是相当的大。
因此他们划分Java原始代码成更小的模块。

- 模块化执行时间的影像

> 这个特性的主要目标是“调整JDK和JRE执行时间的影像系统的支撑”。

- 压缩大部分内部的API

> 这个特性的主要目标是“使大多JDK的内部APIs不能进入，但是留下一些重要，用途广泛的内部的APIs”。

- Java平台模块系统

> 这个特性的主要目标“允许用户创造他们的模块开发他们的应用” .

- jlink ：Java连接器

> 这个jlink工具的主要目标“允许用户在他们的应用中创建可执行文件”。

如果您还是对这些还不清晰，不要担心。
我们将在后边的部分列举一些有用的例子来详细讨论这些概念。


### 当前Java系统的问题？
> 在这个部分，我们将谈论关于“我们为什么需要Java SE 9那模块的系统”手段,也就是当前Java系统的问题所在。

- Java SE 8或更加早期的系统在开发或提供Java应用时有以下问题：

 1. 因为JDK是太大的，对于小设备很难进行等比例缩减。Java SE 8提出了3种紧凑类型解决这个问题：compact1、compact2和compact3。
 但是这个问题并没有得到有效的解决。
 
 
 2. Jar文件，像rt.jar等jar文件太大的以至于不能使用在小设备和应用中。
 
 3. 因为JDK是太大的，我们的应用或设备不能支持更好的平台.
 
 4. 由于修饰符是public的缘故，每个人都可以通过此来进行访问，所以在当前Java系统的封闭性不是很强。
 
 5. 由于JDK,Jre过于庞大，以至于很难进行测试和维护应用。
 
 6. 由于public的关系，Java比较开放。不可避免的能访问象sun.*， *.internal.*等的一些内部不重要的APIs。
 
 7.由于上述关系，用户可以访问到一些内部的api，所以安全性方面也是一个大问题。
  
 8. 应用太大。
 
 9. 组件间的松耦合的支持稍微薄弱。
 
 
> 为了解决所有这些问题， 所以Oracle Corp在Java SE 9中发布了Java SE 9模块系统.

### Java9模块系统特性
 
> Java SE 9模块系统提供以下的便利性：

- 在Java SE 9中分离了JDK， JRE，jar等为更小的模。因此我们可以方便的使用任何我们想要的模块。因此缩减Java应用程序到小设备是非常容易的。

- 更加容易的测试和维护

- 支持更好的平台

- public不再仅仅是public。现在已经支持非常强的封闭性(不用担心，后边我们会用几个例子来解释)。 

- 我们不能再访问内部非关键性APIs了。

- 模块可以非常安全地帮助我们掩藏那些我们不想暴露的内部细节，我们可以得到更好的Security.

- 应用会变的非常小，因为我们可以只使用我们要用的模块。

- 组件间的松耦合变得非常容易。

- 更容易支持唯一责任原则(SRP)。

> 接下来我们会逐个探索这些概念。
    
### 比较JDK 8和JDK 9

>我们知道JDK软件都包含什么。在安装JDK 8软件以后，我们能看到目录中包含bin、jre、lib等解压在Java主目录中。

> 在Jdk9中，Oracle Corp稍微改变了这个文件夹的结构如下所示。

![](modular/jdk8_jdk9_compare.png)


- JDK 8文件夹结构：

![](modular/jdk8_folder.png)

- JDK 9文件夹结构：

![](modular/jdk9_folder.png)


> 我们可以发现在JDK 9中不包含JRE的。
JRE被分离到一个分开的文件夹。
但是JDK 9软件包含一个新的文件夹“jmods”。它包含一套Java 9模块，如下所示。

> 在JDK 9，没有rt.jar和tools.jar文件。

![](modular/jdk9_mods.png)

* 注：截止到今天， “jmods”包含98个模块。在后续的版本也许会增加。
“jmods”文件夹在$ {JAVA_HOME} /jmods中才可用。这些叫JDK Modules. *



## 什么是Java 9模块化？

Java9中的模块是代码、数据和有些资源的自描述的集合。它是一套相关package、type(类、抽象类，接口等)与代码&数据和资源。

每个模块仅包含一套支持唯一责任(功能)原则(SRP)的相关代码和数据。  

![](modular/java9_module_high_level.png)

> Java 9模块系统的主要目标是在Java中支持模块化程序设计。  

后续我们将谈论在“什么是模块描述符”和“如何开发Java模块”

### Java 9模块化系统之母

>到现在为止Java 9模块系统提供了98个模块。
并且Oracle Corp已经分离JDK的jars和Java SE规格到两套Modules里.

- 所有的JDK Mdoules与 “jdk.*”一起启动。

- 所有Java SE规格模块与“java.*”一起启动。

> Java 9模块系统有一个“java.base”模块。它叫作基本的模块。
它是一个独立模块并且没有依赖于所有其他模块。
默认情况下，其他模块依赖于这个模块。


所以“java.base”模块是被成为Java9模块化系统之母。

它是所有JDK模块和用户定义的Modules的默认模块.

### 对比的Java 8和Java9应用

使用Java 5,6,7或者8.，我们已经开发了许多Java应用程序。我们知道Java 8或更早的应用是什么样的，包含什么东西。

简而言之，Java 8的应用会是如下所示：

![](modular/jdk8_application.png)


在Java 8或更早的应用，包的顶级组件是按type类型相关的集合进行分组的。同时他也包含一些静态资源。

Java 9的应用与之前的没有太多的不同。
他只是引入了一个新的组件叫module,如果你要用它就需要将相关联的包集合分组到同一个组中。再有就是module修饰符(“module-info.java”).

应用的其余同初期版本应用一样如下所示。

![](modular/jdk9_application.png)


像Java 8应用一样，顶层组件是package，在Java9中则使用module作为顶层组件。



> 注： 每个Java 9模块有仅有的一个模块和一个模块描述符。不同于Java 8，我们不能在独立的module里面创建多个modules。

 简而言之，我们可以说Java 9模块包含以下主要部分：
 
- one Module

- Module Name

- Module Descriptor(描述符)

- 包集合

- Types和Resources集合


> 在这里资源可能是module-info.java, (Module Descriptor)或其他properties 或者XML。


# 结语

作者：ricky

交流群：244930845


