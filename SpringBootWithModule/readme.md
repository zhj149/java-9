# SpringBoot with Java9

# 环境 
- IDE工具
  > idea2017.2.5

- Java版本 
    ```html
    java version "9"
    Java(TM) SE Runtime Environment (build 9+181)
    Java HotSpot(TM) 64-Bit Server VM (build 9+181, mixed mode)
    ```
- SpringBoot
    > 1.5.8.RELEASE
    
    
# 配置
- ide配置环境为Java9 编译版本，sdk版本等
- pom配置
```xml
<properties>
    <!-- Generic properties -->
    <java.version>9</java.version>
</properties>
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-compiler-plugin</artifactId>
    <version>3.1</version>
    <configuration>
        <source>1.9</source>
        <target>1.9</target>
        <encoding>UTF-8</encoding>
    </configuration>
</plugin>
```





