package com.ricky.entity;

import com.ricky.entity.base.BaseResult;
import com.ricky.inter.LogInter;
import ricky.impl.LogImpl;

import java.io.Serializable;

/**
 * 当做公共返回实体
 * @author baiguantao 
 */
public class ResultBean   extends BaseResult implements Serializable {

    private  String msg;
    private  String code;
    private static  ResultBean resultBean;

    static void init(){
        resultBean=new ResultBean();
    }
    public static   ResultBean getSuccess(){
        if (resultBean==null) {
            init();
        }
        resultBean.setCode("00");
        resultBean.setMsg("success");
        return resultBean;
    }
    public static   ResultBean getError(){
        if (resultBean==null) {
            init();
        }
        resultBean.setCode("01");
        resultBean.setMsg("error");
        return resultBean;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "ResultBean{" +
                "msg='" + msg + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
