package com.ricky.entity.base;

public class BaseResult {
    public static String author="ricky";
    private static String company="rqb";

    public static String getAuthor() {
        return author;
    }

    public static void setAuthor(String author2) {
        author = author2;
    }

    public static String getCompany() {
        return company;
    }

    public static void setCompany(String company2) {
        company = company2;
    }
}
